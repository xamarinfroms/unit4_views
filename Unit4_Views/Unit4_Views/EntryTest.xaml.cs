﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit4_Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryTest : ContentPage
    {
        public EntryTest()
        {
            InitializeComponent();
            Entry txt = new Entry()
            {
                Placeholder = "請輸入文字",
                IsPassword = true,
                Keyboard = Keyboard.Numeric
            };
            txt.TextChanged += Entry_TextChanged;
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
