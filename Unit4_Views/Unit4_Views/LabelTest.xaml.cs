﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit4_Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LabelTest : ContentPage
    {
        public LabelTest()
        {
            InitializeComponent();

            Label lbl = new Label() {
                Text = "i am label",
                TextColor=Color.Red,
                BackgroundColor=Color.Green,
                HorizontalTextAlignment= TextAlignment.Center,
                VerticalTextAlignment= TextAlignment.End
            };
        }
    }
}
