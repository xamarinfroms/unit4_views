﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit4_Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageTest : ContentPage
    {
        public ImageTest()
        {
            InitializeComponent();

           // img.Source = ImageSource.FromResource("Unit4_Views.cat.jpg");

            // img.Source =
            // ImageSource.FromResource("Unit4_Views.cat.jpg");
            //img.Source
            // = new UriImageSource()
            // {
            //     Uri=new Uri("https://xamarin.com/content/images/pages/forms/example-app.png"),
            //      CacheValidity=new TimeSpan(0,5,0)
            // };
        }
    }
    [ContentProperty("Source")]
    public  class ImgResourceExtension : IMarkupExtension
    {
        public string Source { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this.Source != null ?
                 ImageSource.FromResource($"Unit4_Views.{this.Source}") :
                 null;
        }
    }
}
