﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit4_Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WebViewTest : ContentPage
    {
        public WebViewTest()
        {
            InitializeComponent();

            WebView wb = new WebView()
            {
                Source="http://tw.yahoo.com"
            };

            var html = new HtmlWebViewSource()
            {
                Html = @"
                           <html>
                            <body>
                          <h1>XamarinForms</h1>
                          <p>i am WebView YA!</p>
                          </body>
                          </html>"
            };
            this.web.Source = html;
        }
    }
}
